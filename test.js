module.exports = () => {
    // initialization
    const data = require('./data.json');
    let started = 0;
    const millis = () => new Date().getTime();
    const times = {};

    // for

    started = millis();

    for (i = 0; i < data.length; i++) {
        console.log(data[i]);
    }

    times.for = millis() - started;

    // for of

    started = millis();

    for (const i in data) {
        console.log(data[i]);
    }

    times.forof = millis() - started;

    // for in

    started = millis();

    for (i in data) {
        console.log(data[i]);
    }

    times.forin = millis() - started;

    // foreach

    started = millis();

    data.forEach(todo => console.log(todo));

    times.foreach = millis() - started;

    return times;
}
