const test = require('./test.js')
const results = [];

let count = 0;

setInterval(() => {
    if (count > 20) {
        // print as markdown

        results.forEach(result => {
            console.log(`- ${results.indexOf(result)}:`)
            Object.entries(result).forEach(entry => console.log(`   - ${entry[0]}: ${entry[1]}ms`));
        });
        process.exit();
        return;
    }
    results.push(test());
    count++;
}, 500);


